\documentclass{beamer}
\usepackage{nth}
\usepackage[utf8]{inputenc}
\usepackage{minted}
\usepackage{mathtools}


\title{Reading Email in Emacs with mu4e}
\author{David Florness}
\date{February \nth{14}, 2019}


\newcommand*{\tx}{\texttt}


\usetheme{Madrid}
\usecolortheme{beaver}


\begin{document}
\frame{\titlepage}

\begin{frame}{Overview}
  \begin{itemize}
  \item A brief history of email in Emacs
  \item What's \tx{mu4e}?
  \item Why \tx{mu4e}?
  \item \tx{offlineimap}
  \item \tx{msmtp}
  \item \tx{mu\{,4e\}} Installation
  \item \tx{mu} config
  \item Emacs packages config
  \item \tx{mu4e} config
  \item Signing and encrypting with PGP
  \item Demo
  \end{itemize}
\end{frame}

\begin{frame}{A brief history}
  \begin{itemize}
  \item Reading and writing email in Emacs has been a thing for a while.
  \item \tx{Gnus}, a ``news'' and (now) email client, has shipped with Emacs
    since November 1995.
  \item ``News'' in this sense means content from the Usenet network, which is a
    discussion system conceived in 1979 that uses the UUCP (Unix-to-Unix Copy)
    suite of protocols
  \item I'm not a fan because
    \begin{itemize}
    \item the documentation is old and clunky
    \item messages can only be read once, and changing this is an ``uphill
      battle'':
      \begin{quote}
        Gnus does not behave like traditional mail readers.  If you want to make
        it behave that way, you can, but it’s an uphill battle.\footnote{from
          the \tx{Gnus} manual, section 6.4.1}
      \end{quote}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Introducing \tx{mu} and \tx{mu4e}}
  \fontsize{9pt}{7.2}\selectfont
  \begin{itemize}
  \item Created by Dirk-Jan Binnema, djcb:
    \begin{center}
      \includegraphics[width=0.5\textwidth]{djcb.jpeg}
    \end{center}
  \item \tx{mu} is a tool for dealing with e-mail messages stored in the
    Maildir-format, on Unix-like systems. \tx{mu}’s main purpose is to help you
    to find the messages you need, quickly\footnote{from the \tx{mu} website,
      http://www.djcbsoftware.nl/code/mu/}
  \item \tx{mu4e} is simply a front end for Emacs (\tx{mu4e} $\coloneqq$ mu 4
    (for) Emacs)
  \end{itemize}
\end{frame}

\begin{frame}{Why \tx{mu}?}
  \begin{itemize}
  \item From the \tx{mu4e} manual\footnote{\tx{C-h i m mu4e RET}}, section 1.1:
    \begin{quote}
      I (the author) spend a \_lot\_ of time dealing with e-mail, both
      professionally and privately. Having an efficient e-mail client is
      essential. Since none of the existing ones worked the way I wanted, I
      thought about creating my own.
    \end{quote}
  \item Once you get used to it, \tx{mu4e} makes reading and writing email less
    painful (might I say fun?)
  \end{itemize}
\end{frame}

\begin{frame}{\tx{offlineimap}}
  \begin{itemize}
  \item To get started, the first step is to download all of your emails using
    the Internet Message Access Protocol (IMAP); I highly discourage POP3!
  \item If you have a Gmail account (which you do, since its what the Mines
    MyMail accounts are) and want to use it, you'll need to enable ``Insecure
    Apps''.
  \item For Gmail, also create an application-specific password. (recommended)
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\tx{\textasciitilde/.offlineimaprc}}
  \fontsize{9pt}{7.2}\selectfont
  \begin{minted}{ini}
    [general]
    accounts = mines
    pythonfile = ~/.offlineimap.py

    [Account mines]
    localrepository = mines-local
    remoterepository = mines-remote

    [Repository mines-local]
    type = Maildir
    localfolders = ~/mail/mines

    [Repository mines-remote]
    type = Gmail
    remoteuser = davidflorness@mymail.mines.edu
    remotepasseval = get_mines_pass()
    sslcacertfile = /etc/ssl/certs/ca-certificates.crt
    ssl_version = tls1_2
    ssl = yes
  \end{minted}
\end{frame}

\begin{frame}[fragile]{\tx{\textasciitilde/.offlineimap.py}}
  \begin{minted}[breaklines]{python}
    #!/usr/bin/env python2
    from subprocess import check_output
    from getpass import getpass


    def get_mines_pass():
        try:
            return check_output("pass mail/mines/imap", shell=True).splitlines()[0]
        except:
            return getpass("Mines-imap password: ")
  \end{minted}
\end{frame}

\begin{frame}[fragile]{Time to download your email!}
  \begin{itemize}
  \item With the aforementioned configs in place, just run the following to
    download your email:
\begin{verbatim}
  offlineimap -o
\end{verbatim}
  \item This will take a while the first time$\dots$
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\tx{msmtp}}
  \begin{itemize}
  \item Next, you need to set up some way to send your email.
  \item 99\% of the time, this is done with the Simple Mail Transfer Protocol
    (SMTP).
  \item \tx{msmtp} is a very simple STMP client that sends your email and
    nothing more.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\tx{\textasciitilde/.msmtprc}}
  \begin{minted}{ini}
# Always use security
defaults
auth            on
tls             on
tls_trust_file /etc/ssl/certs/ca-certificates.crt
protocol       smtp

# College account
account         college
host            smtp.mines.edu
port            587
from            davidflorness@mines.edu
user            davidflorness
passwordeval    "pass show multipass"
  \end{minted}
\end{frame}

\begin{frame}[fragile]{\tx{mu\{,4e\}} Installation}
  \begin{itemize}
  \item I know no one needs to this, but just in case:
    \begin{minted}{sh}
      pacman -S emacs
    \end{minted}
  \item And install mu itself (this comes with mu4e, as well)
    \begin{minted}{sh}
      pacman -S mu
    \end{minted}
  \item Of course, swap \tx{pacman} with whatever your distro uses (\tx{apt},
    \tx{yum}, \dots) and look up what the package names actually are. If no
    package for \tx{mu} exists for your distro, you can install from source (or
    switch to a better distro).
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\tx{mu} Configuration}
  \begin{itemize}
  \item Tell \tx{mu} where your Maildir is:
    \begin{minted}{sh}
      mu index --rebuild --maildir ~/mail
    \end{minted}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Emacs Package Configuration}
  \fontsize{9pt}{7.2}\selectfont
  \begin{minted}{emacs-lisp}
    (require 'package)

    ;;; Add org and melpa package archives
    (let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                        (not (gnutls-available-p))))
           (melpa-url (concat (if no-ssl "http" "https")
                              "://melpa.org/packages/"))
           (org-url (concat (if no-ssl "http" "https")
                              "://orgmode.org/elpa/")))
      (add-to-list 'package-archives (cons "melpa" melpa-url))
      (add-to-list 'package-archives (cons "org" org-url)))

    ;;; Load and activate lisp packages
    (package-initialize)

    ;;; Fetch the list of available packages
    (unless package-archive-contents
      (package-refresh-contents))

    ;;; Install use-package for easy package configuration
    (unless (package-installed-p 'use-package)
      (package-install 'use-package))

    (require 'use-package)
  \end{minted}
\end{frame}

\begin{frame}[fragile]{\tx{mu4e} Configuration}
  \begin{minted}{emacs-lisp}
    (use-package mu4e
      :defer t
      :commands (mu4e mu4e-compose-new)
      :config
      (progn
        (frypan/setup-mu4e)
        (add-hook 'mu4e-compose-mode-hook
                  'frypan/mu4e-encryption)))

    (use-package evil-mu4e
      :ensure t
      :after mu4e)
  \end{minted}
\end{frame}

\begin{frame}[fragile,allowframebreaks]{Setup functions}
  \fontsize{9pt}{7.2}\selectfont
  \begin{minted}{emacs-lisp}
(defun frypan/setup-mu4e ()
  (setq user-full-name "David Florness"
        mu4e-compose-signature "David\n\nSent from my Emacs"

        ;; SMTP
        message-send-mail-function 'message-send-mail-with-sendmail
        ;; send mail using address in message
        message-sendmail-extra-arguments '("--read-envelope-from")
        message-sendmail-f-is-evil 't
        sendmail-program "msmtp"

        ;; mu4e
        mu4e-maildir "~/mail"
        mail-user-agent 'mu4e-user-agent
        message-kill-buffer-on-exit t
        mu4e-view-show-images t
        mu4e-view-show-addresses t
        mu4e-get-mail-command "offlineimap -o"
        mu4e-headers-include-related nil
  \end{minted}
  \framebreak
  \begin{minted}{emacs-lisp}
;; contexts (i.e. accounts)
mu4e-contexts
`( ,(make-mu4e-context
     :name "college"
     :match-func (lambda (msg)
                   (when msg
                     (or
                      (mu4e-message-contact-field-matches
                       msg
                       :to "davidflorness@mines.edu")
                      (mu4e-message-contact-field-matches
                       msg
                       :to "davidflorness@mymail.mines.edu"))))
     :vars '((mu4e-trash-folder . "/mines/[Gmail].Trash")
             (mu4e-drafts-folder . "/mines/[Gmail].Drafts")
             (mu4e-sent-folder . "/mines/[Gmail].Sent Mail")
             (mu4e-sent-messages-behavior . sent)
             (user-mail-address . "davidflorness@mines.edu")
             (mu4e-maildir-shortcuts .
                                     (("/mines/INBOX" . ?i)
                                      ("/mines/[Gmail].Sent Mail" . ?s)
                                      ("/mines/[Gmail].Trash" . ?t)
                                      ("/mines/[Gmail].All Mail" . ?a)
                                      ("/mines/[Gmail].Drafts" . ?d)
                                      )))))))
  \end{minted}
\end{frame}

\begin{frame}[fragile]{Signing and encrypting with PGP}
  \begin{minted}{emacs-lisp}
;;; setup encryption for mu4e
(defun frypan/mu4e-encryption ()
  ;; always sign messages
  (mml-secure-message-sign-pgpmime)

  (let ((msg mu4e-compose-parent-message))
    (when msg
      (when (member 'encrypted (mu4e-message-field msg :flags))
        ;; encrypt message if replying to encrypted message
        (mml-secure-message-encrypt-pgpmime)))))
  \end{minted}
\end{frame}

\begin{frame}[fragile]{Starting \tx{mu4e}}
  \begin{itemize}
  \item And your configuration is all done!
  \item Now just start \tx{mu4e} with
\begin{verbatim}
  M-x mu4e
\end{verbatim}
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{center}
    \huge{Demo time!}
  \end{center}
\end{frame}

\end{document}